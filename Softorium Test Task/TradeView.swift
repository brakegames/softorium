//
//  TradeView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

struct TradeView: View {
    
    @State private var timer: String = "00:01"
    @State private var investment : String = "1,000"
    @StateObject var apiManager = APIManager.shared
    
    @FocusState private var timerFocused: Bool
    @FocusState private var investmentFocused: Bool
    
    @State var scale = 1.0
    
    var body: some View {
        ZStack {
            Color(red: 0.071, green: 0.086, blue: 0.161)
                .ignoresSafeArea()
            
            VStack(alignment: .center) {
                Text("Trade")
                    .font(.custom("InterVariable", size: 22.0).weight(.bold))
                    .foregroundColor(.white)
                Spacer(minLength: 15)
                HStack {
                    Spacer(minLength: 30.0)
                    ZStack(alignment: .center) {
                        RoundedRectangle(cornerRadius: 12, style: .continuous)
                            .fill(Color(red: 0.2, green: 0.216, blue: 0.286))
                            .frame(height: 54)
                        VStack(alignment: .center) {
                            Text("Balance")
                                .font(.custom("InterVariable", size: 12).weight(.regular))
                                .foregroundColor(Color(red: 0.784, green: 0.784, blue: 0.784))
                                .padding(.top, 5)
                            Spacer()
                            Text("10 000")
                                .font(.custom("InterVariable", size: 16).weight(.semibold))
                                .foregroundColor(.white)
                                .padding(.bottom, 8)
                                .glow()
                        }
                    }
                    .frame(height: 54)
                    Spacer(minLength: 30.0)
                }
                Spacer(minLength: 25)
                WebView(url: (apiManager.selectedPair?.url!)!)
                Spacer(minLength: 16)
                
                NavigationLink {
                    ChoosePairView()
                } label: {
                    HStack {
                        Spacer(minLength: 30.0)
                        ZStack (alignment: .trailing) {
                            ZStack(alignment: .center) {
                                RoundedRectangle(cornerRadius: 12, style: .continuous)
                                    .fill(Color(red: 0.2, green: 0.216, blue: 0.286))
                                    .frame(height: 54)
                                Text(self.apiManager.selectedPair?.name ?? "-")
                                    .font(.custom("InterVariable", size: 16).weight(.bold))
                                    .foregroundColor(.white)
                            }
                            Image("arrow-left")
                                .alignmentGuide(.trailing, computeValue: {
                                    $0[.trailing] + 11
                                })
                        }
                        Spacer(minLength: 30.0)
                    }
                }
                Spacer(minLength: 10)
                HStack {
                    Spacer(minLength: 30.0)
                    ZStack(alignment: .center) {
                        RoundedRectangle(cornerRadius: 12, style: .continuous)
                            .fill(Color(red: 0.2, green: 0.216, blue: 0.286))
                            .frame(height: 54)
                            .overlay {
                                if timerFocused {
                                    RoundedRectangle(cornerRadius: 12)
                                        .stroke(Color(red: 0.208, green: 0.725, blue: 0.447), lineWidth: 1)
                                }
                            }
                        VStack(alignment: .center) {
                            Text("Timer")
                                .font(.custom("InterVariable", size: 12).weight(.regular))
                                .foregroundColor(Color(red: 0.784, green: 0.784, blue: 0.784))
                            HStack {
                                Button {
                                    
                                } label: {
                                    Image("minus-cirlce")
                                }
                                .padding(.leading, 15)
                                Spacer()
                                TextField("timer", text: $timer)
                                    .font(.custom("InterVariable", size: 16).weight(.semibold))
                                    .foregroundColor(.white)
                                    .multilineTextAlignment(.center)
                                    .submitLabel(.done)
                                    .focused($timerFocused)
                                Spacer()
                                Button {
                                    
                                } label: {
                                    Image("add-circle")
                                }
                                .padding(.trailing, 15)
                            }
                        }
                    }
                    Spacer(minLength: 11)
                    ZStack(alignment: .center) {
                        RoundedRectangle(cornerRadius: 12, style: .continuous)
                            .fill(Color(red: 0.2, green: 0.216, blue: 0.286))
                            .frame(height: 54)
                            .overlay {
                                if investmentFocused {
                                    RoundedRectangle(cornerRadius: 12)
                                        .stroke(Color(red: 0.208, green: 0.725, blue: 0.447), lineWidth: 1)
                                }
                            }
                        VStack(alignment: .center) {
                            Text("Investment")
                                .font(.custom("InterVariable", size: 12).weight(.regular))
                                .foregroundColor(Color(red: 0.784, green: 0.784, blue: 0.784))
                            HStack {
                                Button {
                                    
                                } label: {
                                    Image("minus-cirlce")
                                }
                                .padding(.leading, 15)
                                Spacer()
                                TextField("investment", text: $investment)
                                    .font(.custom("InterVariable", size: 16).weight(.semibold))
                                    .foregroundColor(.white)
                                    .multilineTextAlignment(.center)
                                    .submitLabel(.done)
                                    .focused($investmentFocused)
                                Spacer()
                                Button {
                                    
                                } label: {
                                    Image("add-circle")
                                }
                                .padding(.trailing, 15)
                            }
                        }
                    }
                    Spacer(minLength: 30.0)
                }
                HStack {
                    Spacer(minLength: 30)
                    Button {} label: {
                        ZStack(alignment: .topLeading) {
                            RoundedRectangle(cornerRadius: 12, style: .continuous)
                                .fill(Color(red: 0.996, green: 0.239, blue: 0.263))
                                .frame(height: 54)
                            Text("Sell")
                                .font(.custom("InterVariable", size: 24).weight(.medium))
                                .foregroundColor(.white)
                                .padding(.leading, 20)
                                .padding(.top, 8)
                        }
                    }
                    Spacer(minLength: 11)
                    Button { } label: {
                        ZStack(alignment: .topLeading) {
                            RoundedRectangle(cornerRadius: 12, style: .continuous)
                                .fill(Color(red: 0.208, green: 0.725, blue: 0.447))
                                .frame(height: 54)
                            Text("Buy")
                                .font(.custom("InterVariable", size: 24).weight(.medium))
                                .foregroundColor(.white)
                                .padding(.leading, 20)
                                .padding(.top, 8)
                        }
                    }
                    Spacer(minLength: 30)
                }
            }
            .padding(.bottom, 12)
        }
    }
}

struct Glow: ViewModifier {
    @State private var throb = false
    func body(content: Content) -> some View {
        ZStack {
            content
                .blur(radius: throb ? 15 : 0)
                .animation(.easeOut(duration: 1.0).repeatForever(), value: throb)
                .onAppear {
                    throb.toggle()
                }
            content
        }
    }
}

extension View {
    func glow() -> some View {
        modifier(Glow())
    }
}

struct TradeView_Previews: PreviewProvider {
    static var previews: some View {
        TradeView()
    }
}
