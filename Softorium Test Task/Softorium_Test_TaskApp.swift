//
//  Softorium_Test_TaskApp.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

@main
struct Softorium_Test_TaskApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
