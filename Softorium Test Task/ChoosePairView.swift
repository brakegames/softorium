//
//  ChoosePairView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

struct ChoosePairView: View {
    @Environment(\.dismiss) var dismiss
    
    @StateObject var apiManager = APIManager.shared
    
    let columns = [GridItem(.flexible(), spacing: 21), GridItem(.flexible())]
    
    var body: some View {
        ZStack {
            Color(red: 0.071, green: 0.086, blue: 0.161)
                .ignoresSafeArea()
            VStack {
                HStack {
                    Spacer(minLength: 37)
                    LazyVGrid(columns: columns, spacing: 20) {
                        ForEach(apiManager.currencyPairs) { _row in
                            ZStack {
                                Button {
                                    apiManager.selectedPair = _row
                                } label: {
                                    if _row.id == apiManager.selectedPair?.id {
                                        RoundedRectangle(cornerRadius: 12)
                                            .frame(height: 54)
                                            .foregroundColor(.green)
                                    } else {
                                        RoundedRectangle(cornerRadius: 12)
                                            .frame(height: 54)
                                            .foregroundColor(Color(red: 0.2, green: 0.216, blue: 0.286))
                                    }
                                }
                                
                                Text(_row.name!)
                                    .font(.custom("InterVariable", size: 14).weight(.semibold))
                                    .foregroundColor(.white)
                            }
                        }
                    }
                    Spacer(minLength: 37)
                }
                .padding(.top, 36)
                Spacer()
            }
        }
        .navigationBarBackButtonHidden(true)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    self.dismiss()
                }) {
                    Label("", systemImage: "chevron.left")
                        .accentColor(.white)
                }
            }
            ToolbarItem(placement: .principal) {
                Text("Currency pair")
                    .font(.custom("InterVariable", size: 22.0).weight(.bold))
                    .foregroundColor(.white)
            }
        }
        
    }
}

struct ChoosePairView_Previews: PreviewProvider {
    static var previews: some View {
        ChoosePairView()
    }
}
