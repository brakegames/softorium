//
//  ContentView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

struct ContentView: View {
    
    @State private var progress = 0.0
    let timer = Timer.publish(every: 0.01, on: .main, in: .common).autoconnect()
    @State private var shouldNavigate = false
    
    var body: some View {
        NavigationStack {
            ZStack(alignment: .center) {
                Image("background")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
                HStack {
                    Spacer(minLength: 37)
                    CustomProgressView(progress: self.progress)
                    Spacer(minLength: 37)
                }
            }
            .onReceive(timer) { time in
                if progress < 1.0 {
                    if Bool.random() {
                        progress += 0.01
                    }
                } else {
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { granted, error in
                        self.shouldNavigate = true
                    }
                }
            }
            .background(
                NavigationLink(destination: TabsView(),
                               isActive: $shouldNavigate) { EmptyView() }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
