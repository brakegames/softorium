//
//  Models.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 19.03.2024.
//

import Foundation

struct CurrencyPair: Identifiable {
    var id: Int?
    var name:String?
    var url:URL?
}

struct Trader: Identifiable {
    var id:Int?
    var flagName:String?
    var name:String?
    var deposit:Int?
    var profit:Int?
}
