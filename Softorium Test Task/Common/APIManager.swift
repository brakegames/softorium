//
//  APIManager.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 19.03.2024.
//

import Foundation

class APIManager: ObservableObject {
    static let shared = APIManager()
    
    @Published var currencyPairs:[CurrencyPair] = []
    @Published var selectedPair: CurrencyPair?
    
    @Published var traders:[Trader] = []
    
    private init() {
        self.getCurrencyPairs()
        self.getTraders()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            self.updateRandomTraders()
        }
    }
    
    func getCurrencyPairs() {
        let namesArray = [
            "AUD / USD",
            "AZN / USD",
            "AMD / USD",
            "BYN / USD",
            "BGN / USD",
            "BRL / USD",
            "HUF / USD",
            "KRW / USD",
            "VND / USD",
            "HKD / USD",
            "GEL / USD",
            "DKK / USD",
            "AED / USD",
            "KZT / USD"
        ]
        
        let urlsList = [
            "https://www.tradingview.com/chart/?symbol=BMFBOVESPA%3AEUR1%21",
            "https://www.tradingview.com/chart/?symbol=NASDAQ%3AGBP"
        ]
        for (i, name) in namesArray.enumerated() {
            self.currencyPairs.append(CurrencyPair (id: i, name: name, url: URL(string: urlsList.randomElement()!)!))
        }
        self.selectedPair = self.currencyPairs[0]
    }
    
    func getTraders() {
        let namesArray = [
            "Liam",
            "Noah",
            "Oliver",
            "James",
            "Elijah",
            "William",
            "Henry",
            "Lucas",
            "Benjamin",
            "Theodore"
        ]
        
        for (i, name) in namesArray.enumerated() {
            self.traders.append(Trader(
                id: i,
                flagName: "flag_\(Int.random(in: 1...10))",
                name: name,
                deposit: Int.random(in: 100...10000),
                profit: Int.random(in: 5000...500000)
            ))
        }
    }
    
    func updateRandomTraders() {
        let nums = Array(0...self.traders.count - 1)
        
        for _ in 0...Int.random(in: 0...self.traders.count - 1) {
            self.traders[nums.randomElement()!].profit! += Int.random(in: -150...50)
        }
    }
    
    func getSortedTraders() -> [Trader] {
        return self.traders.sorted { $0.profit! > $1.profit! }
    }
}
