//
//  CustomProgressView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

struct CustomProgressView: View {
  let progress: CGFloat

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: 30)
                        .frame(width: geometry.size.width, height: 24)
                        .opacity(0.3)
                        .foregroundColor(Color(red: 0.357, green: 0.353, blue: 0.376))
                    
                    RoundedRectangle(cornerRadius: 30)
                        .frame(
                            width: min(progress * geometry.size.width,
                                       geometry.size.width),
                            height: 24
                        )
                        .foregroundColor(Color(red: 0.208, green: 0.725, blue: 0.447))
                }
                Text("\(Int(progress * 100)) %")
                    .font(.custom("InterVariable", size: 16.0).weight(.heavy))
                    .foregroundColor(.white)
                    
            }
        }
        .frame(height: 24)
    }
}

struct CustomProgressView_Previews: PreviewProvider {
    static var previews: some View {
        CustomProgressView(progress: 0.8)
    }
}
