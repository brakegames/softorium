//
//  TabsView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 18.03.2024.
//

import SwiftUI

struct TabsView: View {
    init() {
        UITabBar.appearance().backgroundColor = UIColor(red: 0.125, green: 0.137, blue: 0.184, alpha: 0.8) // #20232fcc
        UITabBar.appearance().barTintColor = UIColor.green
    }
    var body: some View {
        TabView {
            TradeView()
                .tabItem {
                    Label("Trade", image: "activity")
                }
            TopView()
                .tabItem {
                    Label("Top", image: "user")
                }
        }
        .navigationBarBackButtonHidden(true)
        .accentColor(Color(red: 0.208, green: 0.725, blue: 0.447))
    }
}

struct TabsView_Previews: PreviewProvider {
    static var previews: some View {
        TabsView()
    }
}
