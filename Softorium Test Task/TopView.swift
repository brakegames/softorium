//
//  TopView.swift
//  Softorium Test Task
//
//  Created by EGOR TREPOV on 19.03.2024.
//

import SwiftUI

struct TopView: View {
    
    @StateObject var apiManager = APIManager.shared
    
    @State private var scale = 1.0
    
    let columns = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        ZStack {
            Color(red: 0.071, green: 0.086, blue: 0.161)
                .ignoresSafeArea()
            VStack(alignment: .center) {
                Text("TOP 10 Traders")
                    .font(.custom("InterVariable", size: 22.0).weight(.bold))
                    .foregroundColor(.white)
                    .scaleEffect(scale)
                    .onAppear {
                        withAnimation(.easeInOut(duration: 2.0).repeatForever(autoreverses: true)) {
                            scale = 0.9
                        }
                    }
                ScrollView {
                    VStack(spacing: 0) {
                        ZStack {
                            Rectangle()
                                .foregroundColor(Color(red: 0.18, green: 0.188, blue: 0.243))
                                .frame(height: 50)
                            HStack {
                                Text("№")
                                    .frame(width: 41.5)
                                    .font(.custom("InterVariable", size: 12).weight(.regular))
                                    .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                Text("Country")
                                    .frame(width: 70)
                                    .font(.custom("InterVariable", size: 12).weight(.regular))
                                    .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                Text("Name")
                                    .frame(width: 70)
                                    .font(.custom("InterVariable", size: 12).weight(.regular))
                                    .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                Spacer()
                                Text("Deposit")
                                    .frame(width: 70)
                                    .font(.custom("InterVariable", size: 12).weight(.regular))
                                    .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                Text("Profit")
                                    .frame(width: 70)
                                    .font(.custom("InterVariable", size: 12).weight(.regular))
                                    .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                            }
                        }
                        ForEach(Array(zip(apiManager.getSortedTraders().indices, apiManager.getSortedTraders())), id: \.0) { index, trader in
                            ZStack {
                                Rectangle()
                                    .foregroundColor(index % 2 == 0 ? .clear : Color(red: 0.18, green: 0.188, blue: 0.243))
                                    .frame(height: 50)
                                HStack {
                                    Text("\(index + 1)")
                                        .frame(width: 41.5)
                                        .font(.custom("InterVariable", size: 14).weight(.regular))
                                        .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                    HStack {
                                        Image(trader.flagName!)
                                    }
                                        .frame(width: 70)
                                    Text(trader.name!)
                                        .frame(width: 70)
                                        .font(.custom("InterVariable", size: 14).weight(.regular))
                                        .foregroundColor(.white)
                                    Spacer()
                                    Text("$\(trader.deposit!)")
                                        .frame(width: 70)
                                        .font(.custom("InterVariable", size: 12).weight(.regular))
                                        .foregroundColor(Color(red: 0.757, green: 0.761, blue: 0.784))
                                    Text("$\(trader.profit!)")
                                        .frame(width: 70)
                                        .font(.custom("InterVariable", size: 12).weight(.regular))
                                        .foregroundColor(Color(red: 0.208, green: 0.725, blue: 0.447))
                                }
                            }
                        }
                    }
                    .cornerRadius(8.75)
                }
            }
        }
    }
}

struct TopView_Previews: PreviewProvider {
    static var previews: some View {
        TopView()
    }
}
